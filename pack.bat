@echo off

set _7z="C:\Program Files\7-Zip\7z.exe"

mkdir solution
del solution\solution.zip
del solution\*.txt
%_7z% a -- solution\solution.zip *.py *.bat
call run_all_tests.bat solution/output_