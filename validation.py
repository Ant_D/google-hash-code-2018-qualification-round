def correct_slide(slide):
    if len(slide) == 1:
        return slide[0].orient == 'H'
    elif len(slide) == 2:
        return all(image.orient == 'V' for image in slide)
    return False


def validate_answer(slides):
    image_set = {image.id for slide in slides for image in slide}
    image_cnt = sum(map(len, slides))
    return image_cnt == len(image_set) and \
        all(correct_slide(slide) for slide in slides)
        