from image import Image


def read_test(test_file) -> list:
    images = []
    test_size = int(test_file.readline())
    for id_, line in enumerate(test_file):
        line = line.strip()
        orient, tags_cnt, *tags = line.split(' ')
        tags = set(tags)
        images.append(Image(id_, orient, tags))
        assert(int(tags_cnt) == len(tags))
    assert(test_size == len(images))
    return images


def read_answer(answer_file) -> list:
    slides = []
    answer_size = int(answer_file.readline())
    for line in answer_file:
        line = line.strip()
        slide = list(map(int, line.split(' ')))
        slides.append(slide)
    assert(answer_size == len(slides))
    return slides


def write_answer(slides):
    print(len(slides))
    for slide in slides:
        for image in slide:
            print(image.id, end=" ")
        print()