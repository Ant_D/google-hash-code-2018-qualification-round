import sys
import random
import time

from read_write import read_test, write_answer
from image import Image
from eval_score import eval_score, get_score


def vertical_score(images: list, index: int) -> int:
    neighbor_index = index + 1 - 2 * (index % 2)
    tags = images[index].tags
    neighbor_tags = images[neighbor_index].tags
    return len(tags.union(neighbor_tags))


def neighborhood_score(slides: list, index: int) -> int:
    score = 0
    if index >= 1:
        score += get_score(slides[index - 1], slides[index])
    if index < len(slides) - 1:
        score += get_score(slides[index], slides[index + 1])    
    return score


def improving_swap(array: list, i1: int, i2: int, score: callable) -> None:
    was = score(array, i1) + score(array, i2)
    array[i1], array[i2] = array[i2], array[i1]
    will = score(array, i1) + score(array, i2)
    if will <= was:
        array[i1], array[i2] = array[i2], array[i1]


def improving_shuffle(array: list, n_iter: int, score: callable) -> None:
    if not array:
        return
    for _ in range(n_iter):
        i1 = random.randint(0, len(array) - 1)
        i2 = random.randint(0, len(array) - 1)
        improving_swap(array, i1, i2, score)
    #for _ in range(10):
    #    for i in range(1, len(array)):
    #        improving_swap(array, i - 1, i, score)


def make_vertical_slides(images: list, n_iter: int) -> list:
    images = [image for image in filter(lambda i: i.orient == "V", images)]
    slides = []
    improving_shuffle(images, n_iter, vertical_score)
    for i in range(1, len(images), 2):
        slide = [images[i - 1], images[i]]
        slides.append(slide)
    return slides


def solve(images: list, n_iter: int) -> list:
    hor_slides = [[image] for image in filter(lambda i: i.orient == "H", images)]
    ver_slides = make_vertical_slides(images, n_iter)

    slides = hor_slides + ver_slides
    improving_shuffle(slides, n_iter, neighborhood_score)

    return slides


if __name__ == "__main__":
    images = read_test(sys.stdin)
    slides = solve(images, min(1000000, len(images) ** 2))
    write_answer(slides)