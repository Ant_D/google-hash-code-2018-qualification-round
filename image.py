class Image:
    def __init__(self, id_, orient, tags):
        self.id = id_
        self.orient = orient
        self.tags = tags

    def __repr__(self):
        return "Image({}, {}, {})".format(self.id, self.orient, self.tags)