import sys
import argparse

from image import Image
from read_write import read_test, read_answer
from validation import validate_answer


def parse_args():
    parser = argparse.ArgumentParser(description="Score evaluator")
    parser.add_argument("--test", type=str, help="File with test")
    return parser.parse_args()


def get_tags(slide) -> set:
    tags = set()
    for image in slide:
        tags.update(image.tags)
    return tags


def get_score(prev_slide, cur_slide) -> int:
    prev_tags = get_tags(prev_slide)
    cur_tags = get_tags(cur_slide)
    intersection = prev_tags.intersection(cur_tags)
    return min(len(intersection),
                len(prev_tags) - len(intersection),
                len(cur_tags) - len(intersection))


def eval_score(slides) -> int:
    assert(validate_answer(slides))

    score = 0
    for i in range(1, len(slides)):
        score += get_score(slides[i - 1], slides[i])

    return score


if __name__ == "__main__":
    args = parse_args()
    with open(args.test, "r") as test_file:
        images = read_test(test_file)
    slides = read_answer(sys.stdin)
    slides = [[images[i] for i in slide] for slide in slides]
    score = eval_score(slides)
    print(score)